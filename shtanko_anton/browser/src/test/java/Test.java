

public class Test {
    public static void main(String[] args) {
        String pluginRootDirectory = "C:/ProjectsIdea/SberTraineeShip/lesson7/shtanko_anton/jar/";
        Plugin pluginImpl = new PluginManager(pluginRootDirectory).load("browser", "PluginImpl");
        Plugin plugin1 = new PluginManager(pluginRootDirectory).load("plugin1", "PluginOne");
        Plugin plugin2 = new PluginManager(pluginRootDirectory).load("plugin2", "PluginTwo");
        pluginImpl.doUseFull();
        plugin1.doUseFull();
        plugin2.doUseFull();
    }
}
