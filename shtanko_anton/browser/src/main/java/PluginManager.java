import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;

public class PluginManager {
    private final String pluginRootDirectory;

    public PluginManager(String pluginRootDirectory) {
        this.pluginRootDirectory = pluginRootDirectory;
    }

    public Plugin load(String pluginName, String pluginClassName) {
        Plugin plugin = null;
        File file = new File(pluginRootDirectory);
        File[] jarFiles = file.listFiles();
        for (File jar : jarFiles) {
            String moduleName = jar.getName();
            String[] array = moduleName.split("\\W");
            if (array[0].equals(pluginName)) {
                try (URLClassLoader urlClassLoader = new URLClassLoader(new URL[]{new URL("file:///" + jar.getAbsolutePath())})) {
                    plugin = (Plugin) urlClassLoader.loadClass(pluginClassName).newInstance();
                } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return plugin;
    }

}


